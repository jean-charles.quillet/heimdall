{-# LANGUAGE OverloadedStrings #-}
module Heimdall.Common.Encapsulation where

import Control.Monad.Failable   (Failable(..))
import Data.ByteString.Char8    (unpack)
import Data.Configurable        (Configurable(..))

import Heimdall.Exceptions

data Encapsulation = GREType
                   deriving (Enum, Bounded, Eq, Ord, Show)

instance Configurable Encapsulation where
    serialize GREType = return "gre"
    deserialize "gre" = return GREType
    deserialize str   = failure . InvalidValue $ "Unknown encapsulation type " ++ unpack str

encapTypes :: [Encapsulation]
encapTypes = [minBound..maxBound]
