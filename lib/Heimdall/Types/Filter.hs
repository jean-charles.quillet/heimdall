{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.Filter where

import Data.ByteString               (ByteString)
import Data.Char                     (isSpace)
import Data.Configurable             (Configurable(..))
import Data.Referable                (Referable, VolatileRef)
import Control.Applicative           ((<|>))
import Control.Lens                  (makeLenses)
import Control.Monad.Failable        (failure)
import Network.Socket                (SockAddr)
import Heimdall.Exceptions
import Heimdall.Types.Network

import qualified Data.Attoparsec.ByteString.Char8 as A

data Filter_ policyRef action  = Filter { _filterName         :: FilterName,
                                          _filterLayer        :: FilterDepth,
                                          _filterWantsPartial :: Bool,
                                          _filterDelegate     :: Maybe SockAddr,
                                          _filterConnection   :: Maybe NetworkConnection,
                                          _filterPolicy       :: Maybe policyRef,
                                          _filterDefault      :: action}

instance Configurable FilterDepth where
    serialize TransportLayer   = return "transport"
    serialize ApplicationLayer = return "application"
    serialize InDepth          = return "in-depth"
    serialize DPI              = return "dpi"

    deserialize =
        either (failure . InvalidValue) return . A.parseOnly (transport <|> application <|> inDepth <|> dpi)

      where application = A.skipWhile isSpace >> A.string "application" >> return ApplicationLayer
            transport   = A.skipWhile isSpace >> A.string "transport"   >> return TransportLayer
            inDepth     = A.skipWhile isSpace >> A.string "in-depth"    >> return InDepth
            dpi         = A.skipWhile isSpace >> A.string "dpi"         >> return DPI

type FilterName = ByteString

instance Referable (Filter_ policyRef action)

type FilterRef_ policyRef action = VolatileRef FilterName (Filter_ policyRef action)

data FilterDepth = TransportLayer
                 | ApplicationLayer
                 | InDepth
                 | DPI
                   deriving (Bounded, Enum, Eq, Ord)

filterKeyPrefix :: ByteString
filterKeyPrefix = "config.heimdall.filter."

makeLenses ''Filter_
