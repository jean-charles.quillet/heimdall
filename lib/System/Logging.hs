{-# LANGUAGE CPP, OverloadedStrings #-}
{-# LANGUAGE MultiWayIf             #-}
{- |
Description: Heimdall logging facilities
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module System.Logging  (Level(..),
                        debugM,
                        infoM,
                        noticeM,
                        warningM,
                        errorM,
                        criticalM,
                        emergencyM,
                        init,
                        monitor,
                        setPriority,
                        clearPriority,
                        levelsKey) where

import Prelude            hiding (init)
import Control.Concurrent        (threadDelay)
import Control.Concurrent.Async  (Async, async, AsyncCancelled)
import Control.Exception         (Exception,
                                  fromException,
                                  catch)
import Control.Monad.Failable    (Failable(..))
import Control.Monad.Fix         (fix)
import Control.Monad.IO.Class    (MonadIO, liftIO)
import Data.ByteString.Char8     (ByteString, pack)
import Data.Configurable         (Configurable(..))
import Data.Fn                   ((-.-))
import Data.Typeable             (Typeable)
import Database.Adapter          (Adapter(..),
                                  SubAction(..),
                                  uponChange)
import System.Log.Handler        (setFormatter)
import System.Log.Handler.Simple (streamHandler)
import System.Log.Formatter      (simpleLogFormatter)
import System.IO                 (stderr)
--import System.Log.Handler.Syslog (openlog, Facility(..))
import Text.Read                 (readEither)

import qualified System.Log.Logger as L
import qualified Data.ByteString.Char8 as B

levelsKey :: ByteString
levelsKey = "config.heimdall.logging.levels"

newtype Level = Level { unwrapLevel :: (String, L.Priority) }

data LoggingError = InvalidPriority String
                    deriving (Typeable, Show)

instance Exception LoggingError

instance Configurable Level where
    serialize (Level (name, priority)) = return $ (pack name) <> ":" <> (pack $ show priority)
    deserialize str = do
      let (component, rest) = B.span (/= ':') str
          priorityStr       = B.unpack $ B.drop 1 rest
          ePriority         = readEither priorityStr
      flip (either $ failure . InvalidPriority) ePriority $ \priority ->
             return $ Level (B.unpack component, priority)

#ifndef __DEBUG__
debugM :: (MonadIO m) => String -> String -> m ()
debugM _ _ = return ()
#else
debugM :: (MonadIO m) => String -> String -> m ()
debugM = liftIO -.- L.debugM
#endif

infoM :: (MonadIO m) => String -> String -> m ()
infoM = liftIO -.- L.infoM

noticeM :: (MonadIO m) => String -> String -> m ()
noticeM = liftIO -.- L.noticeM

warningM :: (MonadIO m) => String -> String -> m ()
warningM = liftIO -.- L.warningM

errorM :: (MonadIO m) => String -> String -> m ()
errorM = liftIO -.- L.errorM

criticalM :: (MonadIO m) => String -> String -> m ()
criticalM = liftIO -.- L.criticalM

emergencyM :: (MonadIO m) => String -> String -> m ()
emergencyM = liftIO -.- L.emergencyM

defLevel :: L.Priority
defLevel = L.NOTICE

init :: String -> IO ()
init _name  = do
   stderrHandler <- streamHandler stderr L.DEBUG >>= \lh -> return $
            setFormatter lh (simpleLogFormatter "$time : $loggername : [$prio] $msg")
   L.updateGlobalLogger L.rootLoggerName $ L.setHandlers [stderrHandler]
--  syslogHandler <- openlog name [] DAEMON L.DEBUG
--  L.updateGlobalLogger L.rootLoggerName $ L.setHandlers [syslogHandler]
   L.updateGlobalLogger L.rootLoggerName (L.setLevel defLevel)

monitor :: (Adapter a) => a -> IO (Async ())
monitor db = async $ fix monitor'
    where control loop = do
            levels <- importLevels db
            mapM_ setPriority levels
            infoM "Logging" $ "Monitoring config changes"
            uponChange db [ levelsKey ] $ \_ -> do
                   infoM "Logging" $ "Config change detected"
                   mapM_ clearLevel levels
                   return Done
            loop
          monitor' loop = do
            fix control `catch` \e -> case fromException e of
              Just (_::AsyncCancelled) -> return ()
              Nothing -> do
                errorM "Logging" $ "Monitoring failure: " ++ show e
                threadDelay 2718281
                loop
          clearLevel = clearPriority . fst . unwrapLevel

readLevels :: (Failable m) => [ByteString] -> m [Level]
readLevels = mapM deserialize

importLevels :: (Adapter a, Failable m, MonadIO m) => a -> m [Level]
importLevels db = do
  ls <- getSet db levelsKey
  readLevels ls

setPriority :: Level -> IO()
setPriority (Level (name, priority)) = do
  infoM "Logging" $ "Setting " ++ name ++ " logging to " ++ show priority
  L.updateGlobalLogger name $ L.setLevel priority

clearPriority :: String -> IO ()
clearPriority name@"" =
  -- this is retarded but doing this as a workaround for a problem where the level of the "" logger
  -- doesn't get cleared.. so reset level to default
  L.updateGlobalLogger name $ L.setLevel defLevel
clearPriority name =
  L.updateGlobalLogger name $ L.clearLevel
