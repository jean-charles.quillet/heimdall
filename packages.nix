#
{dev, ...}: self: super: let

in {
  mkDerivation = args : super.mkDerivation (args // {
    doCheck   = !dev;
    doHaddock = !dev;
    enableLibraryProfiling = false;
  });

  ghc = super.ghc;

  heimdall = self.callCabal2nix "heimdall" (
               builtins.filterSource (path: type: type != "directory" ||
                                      baseNameOf path != ".git") ./. ) { };

  # overriden nixpkgs packages we need...

  the-count = self.callCabal2nix "the-count" (
    builtins.fetchGit {
      url    = "https://gitlab.com/codemonkeylabs/the-count";
      rev    = "f709c2cb47b79629a68713513f345970822e3dc5"; }
    ) {};
}
