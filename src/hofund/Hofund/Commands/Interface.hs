{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Hofund - Heimdall CLI interface commands
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Hofund.Commands.Interface where

import Control.Monad                 (void)
import Control.Monad.Except          (runExceptT)
import Control.Monad.Failable        (failableIO)
import Control.Monad.IO.Class        (liftIO)
import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Maybe     (MaybeT(..), runMaybeT)
import Control.Monad.State.Strict    (gets, modify)
import Data.ByteString.Char8         (ByteString, pack, unpack)
import Data.Configurable             (Configurable(..), Serializations, serializations)
import Data.IP                       (IP)
import Data.Maybe                    (fromMaybe)
import Data.Set                      (Set)
import Database.Adapter              (Adapter(..), del)
import Heimdall.Common.Encapsulation (Encapsulation)
import Heimdall.Types                hiding (Action)
import Hofund.Commands.Common
import Hofund.Types
import System.Console.StructuredCLI  hiding (Commands)

import qualified Data.Set as S

intfKey :: IntfName -> ByteString
intfKey (IntfName (intfType, iD)) =
    intfKeyPrefix <> (pack $ show intfType) <> "." <> pack (show iD)

interface :: Commands
interface =
  custom "interface"  "<interface name to configure>" parseIntf always checkIntf >+ do
              command "add" "Add a configuration element to this interface" newLevel >+ do
                basic
                addAddress
                addPeer
                addPolicy
              command "remove" "Remove a previously added configuration element" newLevel >+ do
                removeAddress
                removePeer
                removePolicy
              command "delete" "Delete a configuration element from this interface" newLevel >+ do
                basic
                delACL
                delServiceAddress
                delListenerAddress
                delNode
                delDevice
                delType
                delPeer
                delRPath
                delConnTag
                delLogACL
                delMasquerading
                delTrackStats
              command "set" "Set a given property for this interface" newLevel >+ do
                basic
                setACL
                setServiceAddress
                setListenerAddress
                setNode
                setDevice
                setPeer
                setRPath
                setType
                setConnTag
                setLogACL
                setMasquerading
                setTrackStats
              basic
              enable
              encap
              disable
              destroy
              queueType
              showIntf
              vrfId

checkIntf :: IntfName -> StateM Action
checkIntf intf = do
  let intfType = getIntfType intf
  modify $ \state -> state { inTunnel = intfType == TunnelIntf,
                             inQueue  = intfType == QueueIntf,
                             inVIF    = intfType == VIFIntf,
                             target   = intfKey intf }
  return NewLevel

whenQueue :: StateM Bool
whenQueue = gets inQueue

whenTunnel :: StateM Bool
whenTunnel = gets inTunnel

whenVIF :: StateM Bool
whenVIF = gets inVIF

addPolicy :: Commands
addPolicy =
  custom "policy" hint (parseNameFor policyKeyPrefix hint) always setArg1 >+ do
    param "priority" "<integer priority>" asInt setArg2 >+ do
      custom  "matching" hint' (parseNameFor aclKeyPrefix hint') always $
        \acl -> do
          intfKeyName <- getTarget
          policyName  <- getArg1
          priority    <- getArg2
          let key = intfKeyName <> ".policies"
          db <- gets db
          setMapKVs db key [(priority, pack acl <> " => " <> policyName)]
          return NoAction
            where hint  = "<routing policy to add to this interface>"
                  hint' = "<ACL which should match to select this policy>"

addAddress :: Commands
addAddress = ipAddrOp "add" "ip-addresses" S.insert

addPeerAddress :: Commands
addPeerAddress = ipAddrOp "add" "peer-ip-addresses" S.insert

addPeer :: Commands
addPeer =
  command' "peer" "virtual ethernet interface peer configuration" whenVIF newLevel >+ do
    basic
    addPeerAddress

removeAddress :: Commands
removeAddress = ipAddrOp "remove" "ip-addresses" S.delete

removePeerAddress :: Commands
removePeerAddress = ipAddrOp "remove" "peer-ip-addresses" S.delete

removePeer :: Commands
removePeer =
  command' "peer" "virtual ethernet interface peer configuration" whenVIF newLevel >+ do
    basic
    removePeerAddress

ipAddrOp :: String -> ByteString -> (IP -> Set IP -> Set IP) -> Commands
ipAddrOp opName kwd op =
  command "ip" "internet protocol" newLevel >+ do
    custom "address" "<IP address>" parseIP always $
      \ip -> do
        db <- gets db
        intfKeyName <- getTarget
        value  <- runMaybeT $ getMapKV db intfKeyName kwd
        result <- runExceptT $ do
          addresses  <- S.fromList <$> deserialize (fromMaybe "" value)
          addresses' <- serialize . S.toList $ op ip addresses
          setMapKVs db intfKeyName [(kwd, addresses')]
        either badOp return result
        return NoAction
          where badOp err =
                  liftIO . putStrLn $ "Failed to " ++ opName ++ " IP: " ++ show err

removePolicy :: Commands
removePolicy =
  command "policy" "Enter policy removal command" newLevel >+ do
    param "priority" "<integer priority>" asInt $ \priority -> do
      intfKeyName <- getTarget
      let key = intfKeyName <> ".policies"
      db <- gets db
      void . runMaybeT $ delMapKVs db key [pack $ show priority]
      return NoAction

setServiceAddress :: Commands
setServiceAddress =
  setIntfProperty "service-address" "Set interface service IP address" parseIP whenTunnel

setListenerAddress :: Commands
setListenerAddress =
  setIntfProperty "listener-address" "Set interface listener IP address" parseIP whenTunnel

delServiceAddress :: Commands
delServiceAddress =
  command' "service-address" "Remove interface service address" whenTunnel $ delProperty "service-address"

delListenerAddress :: Commands
delListenerAddress =
  command' "listener-address" "Remove interface listener address" whenTunnel $ delProperty "listener-address"

setACL :: Commands
setACL =
  custom "acl" hint (parseNameFor aclKeyPrefix hint) whenQueue $
    \acl -> do
      db <- gets db
      intfKeyName <- getTarget
      failableIO $ setMapKVs db intfKeyName [("acl", pack acl)]
      return NoAction
          where hint = "<ACL to use as traffic selector for this interface>"

setLogACL :: Commands
setLogACL =
  custom "log-acl" hint (parseNameFor aclKeyPrefix hint) always $
    \acl -> do
      db <- gets db
      intfKeyName <- getTarget
      failableIO $ setMapKVs db intfKeyName [("log-acl", pack acl)]
      return NoAction
          where hint = "<Log traffic matching this ACL on this interface>"

delProperty :: ByteString -> StateM Action
delProperty property = do
  db <- gets db
  intfKeyName <- getTarget
  failableIO $ delMapKVs db intfKeyName [property]
  return NoAction

delACL :: Commands
delACL =
  command' "acl" hint whenQueue $ delProperty "acl"
        where hint = "Remove ACL from this interface"

delLogACL :: Commands
delLogACL =
  command "log-acl" hint $ delProperty "log-acl"
        where hint = "Remove logging ACL from this interface"

setMasquerading :: Commands
setMasquerading =
  command "masquerading" "Activate masquerading on this interface" $ do
    isVIF <- gets inVIF
    if not isVIF
      then liftIO $ putStrLn "Masquerading is currently only supported on virtual interfaces (VIFs)"
      else do
        intfKeyName <- getTarget
        setInterface intfKeyName "masquerading" True
    return NoAction

delMasquerading :: Commands
delMasquerading =
  command "masquerading" "Deactivate masquerading on this interface" $
    delProperty "masquerading"

setTrackStats :: Commands
setTrackStats =
  command "track-stats" "Activate stats tracking on this interface" $ do
    intfKeyName <- getTarget
    setInterface intfKeyName "track-stats" True
    return NoAction

delTrackStats :: Commands
delTrackStats =
  command "track-stats" "Deactivate stats tracking on this interface" $
    delProperty "track-stats"

setIntfProperty :: (Configurable a) => String
                                    -> String
                                    -> Parser StateM a
                                    -> StateM Bool
                                    -> Commands
setIntfProperty property hint parser isEnabled =
  custom property hint parser isEnabled $ \value -> do
    intfKeyName <- getTarget
    setInterface intfKeyName (pack property) value
    return NoAction

setDevice :: Commands
setDevice =
  setIntfProperty "device" hint (parseName hint) always
      where hint = "<network device associated with this interface>"

setType :: Commands
setType =
  setIntfProperty "type" hint (parseName hint) always
      where hint = "<type of network device if configured>"

setPeer :: Commands
setPeer =
  setIntfProperty "peer" hint (parseName hint) whenVIF
    where hint = "<[namespace:]interface name for virtual ethernet type interfaces>"


setRPath :: Commands
setRPath =
  setIntfProperty "rpath" hint parseIntf always
    where hint = "Reverse path interface for return traffic to be redirected to"

encap :: Commands
encap =
  setIntfProperty "encapsulation" hint parseEncap whenTunnel
      where parseEncap node input = do
              keywords :: Serializations Encapsulation <- serializations
              let encaps = unpack . snd <$> keywords
              parseOneOf encaps hint node input
            hint = "<tunnel encapsulation type>"

enable :: Commands
enable =
  command "enable" "Enable interface operation" $ do
    setEnabled True
    return NoAction

disable :: Commands
disable =
  command "disable" "Shutdown interface" $ do
    setEnabled False
    return NoAction

setNode :: Commands
setNode =
  setIntfProperty "node" hint (parseName hint) always
    where hint = "<node on which this interface is localized>"

delNode ::Commands
delNode =
  command "node" "Remove interface from given node" $ delProperty "node"

delDevice ::Commands
delDevice =
  command "device" "<remove association to this network device>" $ delProperty "device"

delType ::Commands
delType =
  command "type" "<remove network device type>" $ delProperty "type"

delPeer :: Commands
delPeer =
  command "peer" "<remove interface peer>" $ delProperty "peer"

delRPath :: Commands
delRPath =
  command "rpath" "<remove reverse path interface>" $ delProperty "rpath"

setConnTag :: Commands
setConnTag =
  setIntfProperty "connection-tag" "<apply this tag to connections over this interface" parseConnTag always

delConnTag :: Commands
delConnTag =
  command "connection-tag" "<remove tagging of connections over this interface" $
    delProperty "connection-tag"

vrfId :: Commands
vrfId =
  setIntfProperty "vrf-id" "<vrf-id to assign interface to>" parseVRFID always

queueType :: Commands
queueType =
  setIntfProperty "service-type" hint parseServiceType whenQueue
      where parseServiceType node input = do
              keywords :: Serializations ServiceType <- serializations
              let serviceTypes = unpack . snd <$> keywords
              parseOneOf serviceTypes hint node input
            hint             = "<queue type of service>"

setEnabled :: Bool -> StateM ()
setEnabled isEnabled = void . runMaybeT $ do
  intfKeyName <- lift getTarget
  lift $ setInterface intfKeyName "enabled" isEnabled

setInterface :: (Configurable a) => ByteString -> ByteString -> a -> StateM ()
setInterface intfKeyName property value = do
  status' <- runExceptT $ do
               str <- serialize value
               db  <- lift $ gets db
               failableIO $ setMapKVs db intfKeyName [(property, str)]
  either (cmdFailure $ "setting " ++ unpack intfKeyName ++ ' ':unpack property) return status'

showIntfs :: Commands
showIntfs =
  command "interfaces" "List all configured interfaces" $ do
    intfs <- getAllIntfs
    printOutput $ mapM_ print intfs
    return NoAction

showOne :: ByteString -> StateM ()
showOne key = do
  let policies = key <> ".policies"
  showObject key Nothing
  liftIO $ putStrLn "policies matching:"
  showObject policies Nothing

showIntf :: Commands
showIntf =
  command "show" "Show interface configuration" $ do
    intfKeyName <- getTarget
    showOne intfKeyName
    return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this interface from the system" $ do
    db <- gets db
    intfKeyName <- getTarget
    del db [intfKeyName]
    return NoAction
