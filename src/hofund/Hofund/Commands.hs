{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Hofund - Heimdall CLI commands
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Hofund.Commands where

import Hofund.Commands.ACL           (acl, showACLs)
import Hofund.Commands.Common        (basic)
import Hofund.Commands.Filter        (filterCmd, showFilters)
import Hofund.Commands.Interface     (interface, showIntfs)
import Hofund.Commands.Logging       (logging)
import Hofund.Commands.Policy        (policy, showPolicies)
import Hofund.Commands.RouteMap      (routeMap, showRouteMaps)
import Hofund.Commands.Stats         (stats)
import Hofund.Commands.Tracking      (tracking)
import Hofund.Types
import System.Console.StructuredCLI  ((>+), command, newLevel)


root :: Commands
root = do
  basic
  config
  showC

config :: Commands
config =
    command "config" "Configuration domain" newLevel >+ do
      acl
      basic
      filterCmd
      interface
      logging
      policy
      routeMap
      stats
      tracking

showC :: Commands
showC = do
  command "show" "Configuration show commands" newLevel >+ do
    basic
    showACLs
    showFilters
    showIntfs
    showPolicies
    showRouteMaps
