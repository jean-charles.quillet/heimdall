{- |
Description: Heimdall Packet Injector
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Injector where

import Control.Monad.Failable         (Failable, failableIO)
import Control.Monad.IO.Class         (MonadIO)
import Heimdall.Interfaces            (getIDB, getIntfM)
import Heimdall.Interface.VIFs        (getVIFIntfName)
import Heimdall.Types
import Heimdall.Packet                (Packet)

inject :: (Failable m, MonadIO m) => Packet -> Int -> m ()
inject pkt vifId = do
  idb <- getIDB $ getVIFIntfName vifId
  intf@Interface{..} <- getIntfM idb
  failableIO $ _intfForwarder pkt intf
