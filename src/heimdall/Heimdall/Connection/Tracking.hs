{-# LANGUAGE OverloadedStrings, BangPatterns, LambdaCase #-}
module Heimdall.Connection.Tracking where

import Control.Concurrent                (ThreadId, threadDelay)
import Control.Concurrent.Async          (Async, async, asyncThreadId, AsyncCancelled, cancel)
import Control.Concurrent.MVar
import Control.Exception                 (SomeException
                                         ,catch
                                         ,finally
                                         ,throwTo
                                         ,fromException)
import Control.Monad                     (void)
import Control.Monad.Except              (runExceptT)
import Control.Monad.Fix                 (fix)
import Control.Monad.Failable            (Failable(..), hoist)
import Control.Monad.IO.Class            (MonadIO, liftIO)
import Control.Monad.Trans.Maybe         (MaybeT(..), runMaybeT)
import Data.ByteString.Char8             (ByteString, unpack)
import Data.Configurable                 (Configurable)
import Data.Default                      (def)
import Data.TTLHashTable                 (TTLHashTable)
import Data.IntMap                       (IntMap)
import Data.Maybe                        (fromJust, fromMaybe, isNothing)
import Heimdall.Common.ObjectModel       (getNameFromKey)
import Heimdall.Connections              (Connection)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel              (deserializeVal, monitor)
import Heimdall.Types
import System.IO.Unsafe                  (unsafePerformIO)
import System.Logging             hiding (monitor)

import qualified Data.HashTable.ST.Basic as Basic
import qualified Data.TTLHashTable       as HT
import qualified Data.IntMap             as IMap

type HashTable = TTLHashTable Basic.HashTable

type ConnTable = HashTable Flow (IntMap Connection)

data Context = Context {
      connTableVar :: MVar ConnTable,
      paramsVar    :: MVar Parameters,
      gcThreadVar  :: MVar ThreadId
    }

data Parameters = Parameters { ttl :: Int,
                               gcEveryNLifeTimes :: Int,
                               maxSize :: Int }

defaultTTL         :: Int
defaultTTL         = 60000 -- 60s default connection timeout

defaultMaxSize     :: Int
defaultMaxSize     = 1000000

defaultGCLifeTimes :: Int
defaultGCLifeTimes = 10

{-# NOINLINE context #-}
context :: Context
context = unsafePerformIO $ do
            ht           <- HT.newWithSettings def { HT.defaultTTL    = defaultTTL,
                                                     HT.maxSize       = defaultMaxSize }
            connTableVar <- newMVar ht
            paramsVar    <- newMVar Parameters { ttl               = defaultTTL,
                                                 gcEveryNLifeTimes = defaultGCLifeTimes,
                                                 maxSize           = defaultMaxSize }
            gcThreadVar  <- newEmptyMVar
            return Context { connTableVar = connTableVar,
                             paramsVar    = paramsVar,
                             gcThreadVar  = gcThreadVar }

runTracking :: (?env::Env) => IO (Async ())
runTracking = do
  gcAsync <- async $ fix $ \next -> (loop >> next) `catch` \case
    e | Just (_::AsyncCancelled) <- fromException e -> return ()
    (_::SomeException)                              -> next
  a <- async $ do
    monitor "Connection.Tracking" [connTrackKeyPrefix] configChange configChange
    `finally`
    cancel gcAsync
  let Context {..} = context
  void $ tryTakeMVar gcThreadVar
  putMVar gcThreadVar $ asyncThreadId gcAsync
  return a
    where loop = do
            let Context {..} = context
            Parameters {..} <- readMVar paramsVar
            let delay = gcEveryNLifeTimes * ttl
            infoM "Connection.Tracking" $ "Scheduling garbage collection in " ++ show delay ++ " mS"
            threadDelay $ delay * 1000
            infoM "Connection.Tracking" $ "Starting garbage collection"
            left <- withMVar connTableVar $ HT.removeExpired
            infoM "Connection.Tracking" $ show left ++ " entries remaining to be GC'd"

configChange :: (?env::Env) => ByteString -> IO ()
configChange key = do
  let Context {..} = context
  update name
  withMVar gcThreadVar $ \gcThread ->
    throwTo gcThread WakeUp
      where name = getNameFromKey key

reconfigureTableWith :: (HT.Settings -> HT.Settings) -> HashTable k v -> IO ()
reconfigureTableWith mutator ht = do
  settings <- HT.getSettings ht
  HT.reconfigure ht $ mutator settings

update :: (?env::Env) => ByteString -> IO ()
update name@"ttl" =
  updateTableSetting name updateTTL setTTL
      where updateTTL ttl s = s { HT.defaultTTL = fromMaybe defaultTTL ttl }
            setTTL ttl p    = p { ttl = fromMaybe defaultTTL ttl }
update name@"max-size" =
  updateTableSetting name updateMaxSize setMaxSize
      where updateMaxSize val s = s { HT.maxSize = fromMaybe defaultMaxSize val }
            setMaxSize val p    = p { maxSize = fromMaybe defaultMaxSize val }
update name@"gc-lifetimes" =
  updateTableSetting name (const id) setGCLifeTimes
      where setGCLifeTimes val p = p { gcEveryNLifeTimes = fromMaybe defaultGCLifeTimes val }
update name =
  warningM "Connection.Tracking" $ "Unknown configuration key " ++ unpack name

updateTableSetting :: (?env::Env, Configurable a)
                      => ByteString
                      -> (Maybe a -> HT.Settings -> HT.Settings)
                      -> (Maybe a -> Parameters -> Parameters)
                      -> IO ()
updateTableSetting name updateSettings updateParams = do
  val <- runMaybeT $ deserializeVal connTrackKeyPrefix name `recover` \e -> do
                       noticeM "Connection.Tracking" $ "No configuration (or invalid) " ++ unpack name
                         ++ " found. Reverting to defaults"
                       failure e
  let Context{..} = context
      updateVal   = reconfigureTableWith $ updateSettings val
  liftIO $ reconfigureTables updateVal
  modifyMVar_ paramsVar $ return . updateParams val

reconfigureTables :: (?env::Env)
                   => (ConnTable -> IO ())
                   -> IO ()
reconfigureTables = withMVar connTableVar
  where Context {..} = context

lookupConnection :: (MonadIO m, Failable m) => Maybe VRFID -> Flow -> m (Maybe Connection)
lookupConnection mVRFID flow = liftIO $ do
  let Context {..} = context
  withMVar connTableVar $ \connTable -> runMaybeT $ do
    vrfMap <- HT.lookupAndRenew connTable flow
    hoist id $ connIn vrfMap
      where connIn | isNothing mVRFID = fmap snd . IMap.lookupMin
                   | otherwise        = IMap.lookup  (fromIntegral $ fromJust mVRFID)

updateConnection :: (MonadIO m, Failable m)
                    => VRFID
                    -> Flow
                    -> (Maybe Connection -> Maybe Connection)
                    -> m (Maybe Connection)
updateConnection vrfID flow mutate = do
  let Context {..} = context
  result <- liftIO . modifyMVar connTableVar $ \connTable -> do
             result <- runExceptT $ HT.mutate connTable flow connection
             return (connTable, result)
  hoist id result
      where connection mVRFMap0 =
              let vrfMap0 = fromMaybe IMap.empty mVRFMap0
                  mConn0  = IMap.lookup vrfInt vrfMap0
                  mConn   = mutate mConn0
                  vrfMap  = case mConn of
                              Nothing   -> IMap.delete vrfInt vrfMap0
                              Just conn -> IMap.insert vrfInt conn vrfMap0
                  mVRFMap = if IMap.null vrfMap
                              then Nothing
                              else return vrfMap
              in (mVRFMap, mConn)
            vrfInt = fromIntegral vrfID
