{-# LANGUAGE CPP, InterruptibleFFI, OverloadedStrings #-}

module Heimdall.Interface.Queues where

import Control.Concurrent               (threadDelay)
import Control.Concurrent.Async         (Async, AsyncCancelled, async)
import Control.Exception                (SomeException,
                                         Handler(..),
                                         bracket,
                                         bracket_,
                                         catch,
                                         catches,
                                         mask,
                                         throw)
import Control.Monad                    (forM_, forever, void)
import Control.Monad.Except             (runExceptT)
import Control.Monad.Trans.Maybe        (MaybeT(..), runMaybeT)
import Data.ByteString.Char8            (ByteString, pack, unpack)
import Data.Configurable                (deserialize)
import Data.Maybe                       (fromMaybe)
import Data.Referable                   (ReferableException, watchRef)
import Foreign.C.String                 (withCString)
import Foreign.Ptr                      (Ptr)
import Foreign.StablePtr                (StablePtr, newStablePtr, deRefStablePtr, freeStablePtr)
import Foreign.Storable                 (peek)
#ifdef LINUX
import Heimdall.Interface.Queue.Linux
#else
import Heimdall.Interface.Queue.Generic
#endif
import Heimdall.Interface.Queue.Types
import Heimdall.ACLs                    (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces
import Heimdall.Interface.Utils         (importIntf, selectRP)
import Heimdall.Packet                  (Packet)
import Heimdall.Routing                 (routePacket)
import Heimdall.Types
import System.Logging


import qualified Heimdall.Routing    as Routing

foreign import ccall interruptible "heimdall.h run_queue" run_queue :: StablePtr Queue ->
                                                                       Ptr HuvudQueue ->
                                                                       IO ()

foreign export ccall cAnalyze :: StablePtr Queue -> Ptr Packet -> IO Int

queueConfigPrefix :: ByteString
queueConfigPrefix = intfKeyPrefix <> "queue."

-- keep in sync with verdict_t in huvud.h
data Verdict = Invalid
             | AcceptPacket
             | DropPacket
             | StealPacket
             | WorkingOnIt
             | AbortProcessing
               deriving Enum

cAnalyze :: StablePtr Queue -> Ptr Packet -> IO Int
cAnalyze qPtr pktPtr = do
  q@Queue{..} <- deRefStablePtr qPtr
  analyze q `catch` \(e::SomeException) -> do
    infoM "Queues" $ "Failed to analyze packet on " ++ qName ++ ":" ++ show e
    return $ fromEnum AbortProcessing
  where analyze Queue{..} = do
          pkt     <- peek pktPtr
          action  <- routePacket qIDB (selectRP qIDB) pkt
          let verdict = verdictForAction action
          debugM "Queues" $ qName ++ " <- " ++ show pkt ++ ": " ++ show action
                     ++ '(':show verdict ++ ")"
          return verdict

verdictForAction :: Routing.Result -> Int
verdictForAction Routing.Bypass = fromEnum AcceptPacket
verdictForAction Routing.Drop   = fromEnum DropPacket
verdictForAction Routing.Routed = fromEnum DropPacket
verdictForAction Routing.Abort  = fromEnum AbortProcessing
verdictForAction Routing.None   = fromEnum DropPacket


runQueues :: (?env::Env) => IO (Async ())
runQueues = async $
  runInterfaces "Queues" [queueConfigPrefix] QueueIntf runQ stopQ
    where runQ name info = do
            let node = fromMaybe localNode $ lookup "node" info
            if localNode == node
              then Just <$> runQ' name info
              else return Nothing
          runQ' name info = do
            infoM "Queues" $ "Running " ++ show name
            async $
              void . forever $ serve name info `catches` [
                           Handler $ \(e::ReferableException) ->
                             infoM "Queues" $ show name ++ " servicing restarted: " ++ show e,
                           Handler $ \(e::AsyncCancelled) -> do
                             infoM "Queues" $ show name ++ " servicing stopped: " ++ show e
                             throw e,
                           Handler $ \(e::SomeException) -> do
                             warningM "Queues" $ "Queue " ++ show name ++
                                          " servicing interrupted: " ++ show e
                             threadDelay 1500000]
          stopQ _ =
            return ()
          localNode = pack $ getNode ?env

serve :: (?env::Env) => IntfName -> [(ByteString, ByteString)] -> IO ()
serve name info =
  bracket initQ releaseQ $ \qPtr -> do
    queue@Queue{..} <- deRefStablePtr qPtr
    infoM "Queues" $ "Starting " ++ qName
    let ?queue = queue
    intf <- importQ name info
    registerIntf intf
    forM_ qACL $ watchRef mempty
    bracket_ platformQueueStart platformQueueStop $ do
      infoM "Queues" $ "Servicing " ++ qName ++ " @" ++ show qHQ ++ '(':show qHId ++ ")"
      run_queue qPtr qHQ
      infoM "Queues" $ "Exiting server for queue #" ++ show qId
                        ++ "(HW queue " ++ show qHId ++ ")"
        where initQ = mask $ \_ -> do
                aclRef <- mapM getACLRef $ lookup "acl" info
                idb <- getIDB name
                let dev  = unpack . fromMaybe "" $ lookup "device" info
                    bpf  = unpack . fromMaybe "" $ lookup "filter" info
                serviceType <- fromMaybe ForwardingService <$> getServiceType info
                withCString "Queues" $ \component ->
                  withCString dev $ \device ->
                    withCString bpf $ \filterExp -> do
                      let iD = getIntfID name
                      (hwId, hq) <- platformQueueInit iD component device filterExp
                      newStablePtr Queue { qId          = iD,
                                           qHId         = hwId,
                                           qDev         = dev,
                                           qBpf         = bpf,
                                           qName        = show name,
                                           qIDB         = idb,
                                           qServiceType = serviceType,
                                           qACL         = aclRef,
                                           qHQ          = hq }
              releaseQ qPtr = mask $ \_ -> do
                Queue{..} <- deRefStablePtr qPtr
                platformQueueRelease (qHId, qHQ)
                freeStablePtr qPtr
                infoM "Queues" $ "Destroyed " ++ qName

importQ :: (?env::Env) => IntfName -> [(ByteString, ByteString)] -> IO Interface
importQ name info = do
  importIntf name info deadEnd $ can RxData
      where deadEnd = throw . InternalError $ "Packet hit a dead end on interface " ++ show name

getServiceType :: [(ByteString, ByteString)] -> IO (Maybe ServiceType)
getServiceType info = runMaybeT $ do
  str <- MaybeT . pure $ lookup "service-type" info
  res <- runExceptT $ deserialize str
  MaybeT $ either (noParse str) (return . Just) res
      where noParse str err = do
              infoM "Queues" $ "Invalid service type " ++ show str ++ ": " ++ show err
              return Nothing
