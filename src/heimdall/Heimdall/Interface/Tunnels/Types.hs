module Heimdall.Interface.Tunnels.Types where

import Control.Concurrent.MVar         (MVar)
import Data.IP                         (IP)
import Heimdall.Common.Encapsulation   (Encapsulation)
import Heimdall.Packet                 (Packet)
import Heimdall.Types                  (IDB, Interface)
import Network.Socket                  (Socket)

data Tunnel = Tunnel { tId              :: Int,
                       tName            :: String,
                       tIDB             :: IDB,
                       tEncap           :: Encapsulation,
                       tIsLocal         :: Bool,
                       tServiceAddress  :: Maybe IP,
                       tListenerAddress :: Maybe IP,
                       tSockVar         :: MVar Socket,
                       tForwarder       :: Tunnel -> Packet -> Interface -> IO () }
