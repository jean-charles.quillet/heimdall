module Heimdall.Interface.VIF.Types where

import Control.Concurrent.MVar  (MVar)
import Data.ByteString          (ByteString)
import Data.IP                  (IP)
import Heimdall.Types           (IDB)
import Network.Socket           (Socket)

data VIF = VIF { vifId          :: Int,
                 vifDevName     :: Maybe ByteString,
                 vifDevType     :: Maybe ByteString,
                 vifDevPeer     :: Maybe ByteString,
                 vifDevIPs      :: [IP],
                 vifDevPeerIPs  :: [IP],
                 vifIDB         :: IDB,
                 vifSkVar       :: MVar DualSocket }

newtype DualSocket = DualSocket { unwrapDualSocket :: (Socket, Socket) }
